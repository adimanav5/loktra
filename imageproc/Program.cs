﻿using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;

namespace imageproc
{
    class Program
    {
        static int[] ReadFile(string file)
        {
            int[] image = null;
            using (TextReader reader = File.OpenText(file))
            {
                var imageText = reader.ReadLine();

                var imageArr = imageText.Split(' ', '\t');

                image = new int[imageArr.Length];

                for (int i = 0; i < imageArr.Length; i++)
                {
                    image[i] = int.Parse(imageArr[i]);
                }
            }
            return image;
        }

        static void WriteFile(int[] image, string file)
        {
            using (TextWriter writer = new StreamWriter(file))
            {
                bool first = true;
                foreach (var el in image)
                {
                    string str = string.Empty;
                    if (!first) str += " ";
                    str += el;
                    writer.Write(str);
                    first = false;
                }
            }
        }

        static object Lock { get; set; }

        static void childThread(Object threadArg)
        {
            ThreadArg arg = threadArg as ThreadArg;

            for (int i = 0 + arg.ThreadID; i < arg.InputImage1.Length; i += NumThreads)
            {
                arg.OutputImage[i] = (arg.InputImage1[i] + arg.InputImage2[i])/2;
                
                lock (Lock)
                {
                    if (MaxValue < arg.OutputImage[i])
                    { 
                        lock (Lock)
                        {
                            MaxValue = arg.OutputImage[i];
                        }
                    }
                }
            }
        }

        public static int MaxValue { get; set; }

        public static int NumThreads { get; set; }

        class ThreadArg
        {
            public int[] InputImage1 { get; set; }
            public int[] InputImage2 { get; set; }
            
            public int[] OutputImage { get; set; }
            public int ThreadID { get; set; }
        }

        private static double multiplier = 0;
        static double Multiplier
        {
            get 
            {
                if (multiplier == 0)
                {
                    double tmp = 1.0; 
                    multiplier = tmp * 255 / MaxValue;
                }
                return multiplier;
            }
        }

        static void Normalize(int[] image)
        {
            for (int i = 0; i < image.Length; i++) 
                image[i] = Convert.ToInt32(image[i] * Multiplier);
        }

        static void Main(string[] args)
        {
            if (args.Length < 3) 
            {
                Console.WriteLine("Need to provide number of threads, text file paths for input images and output image!");
                return;
            }

            NumThreads = int.Parse(args[0]);
            var inputFile1 = args[1];
            var inputFile2 = args[2];
            var outputFilePath = args[3];

            var image1 = ReadFile(inputFile1);
            var image2 = ReadFile(inputFile2);
            var outImage = new int[image1.Length];
            MaxValue = int.MinValue;
            Lock = new Object();

            var threads = new Thread[NumThreads];

            for (int i = 0; i < NumThreads; i++)
            {
                threads[i] = new Thread(new ParameterizedThreadStart(childThread));
                threads[i].Start(new ThreadArg{ 
                    InputImage1 = image1,
                    InputImage2 = image2,
                    OutputImage = outImage,
                    ThreadID = i,
                    });
            }

            for (int i = 0; i < NumThreads; i++)
            {
                threads[i].Join();
            }

            Normalize(outImage);

            WriteFile(outImage, outputFilePath);
        }
    }
}
